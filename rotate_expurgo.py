#!/bin/python
import tarfile
import subprocess
import os
import datetime

root_dir = [ 
     [ "/opt/conectpark/", "/opt/Conectpark.SituacaoTag/", "/opt/Conectpark.EnvioMensalistas/", "/opt/Conectpark.EnvioTransacoesPendentes/", "/opt/Conectpark.EnvioTransits/", "/opt/Conectpark.TagsInativas/"],
     ["conectPark.log", "ConectPark.SituacaoTag.log", "Conectpark.EnvioMensalistas.log", "Conectpark.EnvioTransacoesPendentes.log", "Conectpark.EnvioTransits.log", "Conectpark.TagsInativas.log"]
]

def compacta(dir, file, servico):
    today = datetime.date.today()
    with tarfile.open(dir + "-" + str(today) + ".tar.gz", "w:gz") as tar:
        tar.add(dir, arcname=file)
        os.remove(dir)
        nm_folder = conectpark if file == "conectpark.log" else file.split(".")[1]
    if not os.path.exists("/opt/LOGS/" + nm_folder):
        os.makedirs("/opt/LOGS/" + nm_folder)
    os.rename(dir + "-" + str(today) + ".tar.gz", "/opt/LOGS/" + nm_folder + "/" + file + "-" + str(today) + ".tar.gz")
    if not servico == "conectpark.service":
        subprocess.call(["systemctl", "restart", servico])
    return;

for x in root_dir[0]:
        idx_path = root_dir[0].index(x)
        archive = root_dir[1][idx_path]
        full_path = x + root_dir[1][idx_path]
        service = x.split("/")[2] + ".service"
	if (os.path.getsize(full_path)) / 1048576 >= (0):
            compacta(full_path, archive, service)

