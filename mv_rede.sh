#!/bin/bash

cd /unidade/

ls /unidade/ | grep ifcfg | xargs -I {} cp -prf {} /mnt/root/etc/sysconfig/network-scripts/

mv /mnt/root/etc/sysconfig/network-scripts/ifcfg-enp3s0 /mnt/var
