#!/bin/python
#Autor: Diogo Souza

#Data: 26/10/2018

import tarfile
import subprocess
import os
import datetime

today = datetime.datetime.now()
now = today.strftime('%Y-%m-%d')

root_dir = [ "/opt/conectpark/", "/opt/Conectpark.SituacaoTag/", "/opt/Conectpark.EnvioMensalistas/",
	     "/opt/Conectpark.EnvioTransacoesPendentes/", "/opt/Conectpark.EnvioTransits/", "/opt/Conectpark.TagsInativas/"]

def compacta(dir):
    for files in os.listdir(dir):
        if files.endswith(".log"): 
            with tarfile.open(dir + files + "-" + now + ".tar.gz", "w:gz") as tar:
                tar.add(dir + files, arcname= files)
            os.remove(dir + files)
            nm_folder = "conectpark" if files == "conectPark.log" else files.split(".")[1]
            if not nm_folder == ('conectpark'):
                subprocess.call(['systemctl', 'restart', 'Conectpark.' + nm_folder])
            else:
                subprocess.call(['systemctl', 'restart', 'conectpark'])
            if not os.path.exists("/opt/LOGS/" + nm_folder):
                os.makedirs("/opt/LOGS/" + nm_folder)
            os.rename(dir + files + ".tar.gz", "/opt/LOGS/" + nm_folder + "/" + files + ".tar.gz")
    return;


for x in root_dir:
    for y in os.listdir(x):
        if y.endswith(".log"):
	    if (os.path.getsize(x + "/" + y)) / 1048576 >= (100):
		compacta(x)


if (os.path.getsize("/var/log/messages")) / 1048576 >= (100):
    with tarfile.open("/var/log/messages" + "-" + now + ".tar.gz", "w:gz") as tar:
        tar.add("/var/log/messages", arcname= "messages")
    os.remove("/var/log/messages")
    subprocess.call(['systemctl', 'restart', 'rsyslog'])
