#!/bin/python

import os
import shutil

src_dir = "/unidade/"
target_dir = "/mnt/root/etc/sysconfig/network-scripts/"
lista = [ ]
arc = [ ]

for files in os.listdir(src_dir):
    if files.startswith("ifcfg"):
        for y in arc:
            with open(src_dir + y, "r") as archive:
                for lines in archive.readlines():
                    if lines.startswith("BOOTPROTO") or lines.startswith("DEVICE") or lines.startswith("NAME") or lines.startswith("TYPE") or lines.startswith("ONBOOT") or lines.startswith("IPADDR") or lines.startswith("GATEWAY") or lines.startswith("DNS1") or lines.startswith("NETMASK"):
                        lista.append(lines)
            with open(src_dir + y, "w") as novo:
                for z in lista:
                    novo.write(z)
