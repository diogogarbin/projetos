#!/bin/bash



lk_slt=`readlink /mnt/opt/slt | gawk -F/ '{print \$(NF)}'`
lk_conectpark=`readlink /mnt/opt/conectpark | gawk -F/ '{print \$(NF)}'`
lk_EnvioMensalistas=`readlink /mnt/opt/Conectpark.EnvioMensalistas | gawk -F/ '{print \$(NF)}'`
lk_EnvioTransacoesPendentes=`readlink /mnt/opt/Conectpark.EnvioTransacoesPendentes | gawk -F/ '{print \$(NF)}'`
lk_EnvioTransits=`readlink /mnt/opt/Conectpark.EnvioTransits | gawk -F/ '{print \$(NF)}'`
lk_SituacaoTag=`readlink /mnt/opt/Conectpark.SituacaoTag | gawk -F/ '{print \$(NF)}'`
lk_TagsInativas=`readlink /mnt/opt/Conectpark.TagsInativas | gawk -F/ '{print \$(NF)}'`
lk_TagsAtivas=`readlink /mnt/opt/Conectpark.TagsAtivas | gawk -F/ '{print \$(NF)}'`

cd /mnt/opt

rm -f slt ; ln -sf $lk_slt slt
rm -f conectpark ; ln -sf $lk_conectpark conectpark
rm -f Conectpark.EnvioMensalistas ; ln -sf $lk_EnvioMensalistas Conectpark.EnvioMensalistas
rm -f Conectpark.EnvioTransacoesPendentes ; ln -sf $lk_EnvioTransacoesPendentes Conectpark.EnvioTransacoesPendentes
rm -f Conectpark.EnvioTransits ; ln -sf $lk_EnvioTransits Conectpark.EnvioTransits
rm -f Conectpark.SituacaoTag ; ln -sf $lk_SituacaoTag Conectpark.SituacaoTag
rm -f Conectpark.TagsInativas ; ln -sf $lk_TagsInativas Conectpark.TagsInativas
rm -f Conectpark.TagsAtivas ; ln -sf $lk_TagsAtivas Conectpark.TagsAtivas

chown -R mysql. /mnt/var/lib/mysql

