#!/bin/python
# -*- coding: utf8 -*-
from __future__ import unicode_literals

import sys
import time
import datetime
from os.path import isfile, join
import os
import logging 
reload(sys)
sys.setdefaultencoding("utf8")

logging.basicConfig(filename='/opt/reports/expurgo.log', level=logging.INFO,
                    format='%(asctime)s:%(levelname)s:%(message)s')

def expurgo(name_file):

    now = time.time()
    crt_time = os.path.getctime(name_file)
    delta = datetime.datetime.utcfromtimestamp(now - crt_time).strftime("%d")

    if int(delta) >= 180:
        logging.info(name_file + ' ' + 'Foi excluído por ter mais de 6 meses')
        os.remove(name_file)
    return;

root_dir = [ "/opt/LOGS/", "/var/log/" ]

list_files = [ ]
for z in root_dir:

    for path, subdirs, files in os.walk(root_dir):
        for name in files:
            list_files.append(os.path.join(path, name))
    for x in list_files:
        expurgo(x)

