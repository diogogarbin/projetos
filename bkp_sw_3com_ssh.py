#!/bin/python3


# MODULES IMPORT

import paramiko
import time
import datetime
from cryptography.fernet import Fernet



with open('cipher.key', 'rb') as dcrpt:
    chave = dcrpt.read()
key = bytes(chave).decode("utf-8")
cipher_suite = Fernet(key)

with open('sw.key', 'rb') as arq_sw:
    sw_key = arq_sw.read()
plain_sw = (cipher_suite.decrypt(sw_key))
plain_sw_strg = bytes(plain_sw).decode('utf-8')


with open('ftp.key', 'rb') as arq_ftp:
    ftp_key = arq_ftp.read()
ciphered_ftp = ftp_key

plain_ftp = (cipher_suite.decrypt(ciphered_ftp))
pwd_ftp = bytes(plain_ftp).decode("utf-8")
ftp_cnt = paramiko.SSHClient()
ftp_cnt.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ftp_cnt.connect('192.168.10.99', username='root', password=pwd_ftp)


# VARIABLES

utc = datetime.datetime.now()
utc = utc.strftime('%d-%m-%y-%f')
arquivo_cfg = (utc + '.cfg')
delarq = datetime.datetime.now()
delarq = delarq.strftime('%d-%m-%y-')
trash_del = ('delete' + ' ' + delarq + '*')
dt = datetime.datetime.now()
dt = dt.strftime('%d-%m-%y-%H:%M:%s')


# LOOPING

ip = []
nome = []
contador = int(0)

with open('hosts.txt', 'r') as file:
    for text in file.readlines():
        conteudo = text.split(';')
        ip.append(conteudo[1].strip('\n').strip(" ").strip('\t'))
        nome.append(conteudo[0])


    for x in ip:
        y: str
        y = nome[contador]
        contador = contador + 1


        # SSH
        try:

            x = str(x)
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(x, username='admin', password=plain_sw_strg)
            remote = ssh.invoke_shell()

        except:

            with open('/var/log/log_bkp_sw_erro.txt', '+a') as conexao:
                conexao.write(dt + ' ' + 'Problemas de conexão com ' + ' ' + y + '\n')

        else:

            remote.send('_cmdline-mode on' + '\r\n')
            time.sleep(0.1)
            remote.send('y' + '\r\n')
            time.sleep(0.1)
            remote.send('512900' + '\r\n')
            time.sleep(0.1)
            remote.send('save' + '\r\n')
            time.sleep(0.1)
            remote.send('y' + '\r\n')
            time.sleep(0.1)
            remote.send(arquivo_cfg + '\r\n')
            time.sleep(40.0)
            cmd = ('backup startup-configuration to 192.168.10.99' + ' ' + y.strip('\t').strip(' ') + '.cfg')
            remote.send(cmd + '\r\n')
            time.sleep(5.0)
            trash_del = ('delete' + ' ' + delarq + '*')
            remote.send(trash_del + '\r\n')
            time.sleep(0.1)
            remote.send('y' + '\r\n')

            # END

            ssh.close()

# VALIDAÇÃO

output = ''
stdin, stdout, stderr = ftp_cnt.exec_command('find /var/lib/tftpboot/ -maxdepth 1 -mtime +1 | grep cfg')
output = stdout.readlines()
if not output == ' ':
    with open('/var/log/log_bkp_sw.txt', '+w') as log_bkp_sw:
        log_bkp_sw.write('1')
for arquivos in output:
    with open('/var/log/log_bkp_sw_erro.txt', '+a') as erro_sw:
        erro_sw.write('timeout em ' + ' ' + arquivos)
