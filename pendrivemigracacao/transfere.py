#!/bin/python

import os
import shutil


target = [
           ["/mnt/opt/conectpark/", ["config.xml", "certutil", ".config"]], ["/mnt/root/etc/", ["issue", "motd", "openvpn", "resolv.conf", "hosts"]], ["/mnt/opt/slt/", ["configpista", "configslt"]], ["/mnt/var/www/lighttpd/conectpark/conectpark_cli/includes/", ["config.php"]], ["/mnt/opt/", [ "slt2", "local915" ]], ["/mnt/opt/script/",[".conveniado", ".conv" ]], ["/mnt/opt/Conectpark.EnvioTransacoesPendentes/", ["config.xml"]], ["/mnt/opt/Conectpark.EnvioTransits/", ["config.xml"]], ["/mnt/opt/Conectpark.SituacaoTag/", ["config.xml"]], ["/mnt/opt/Conectpark.TagsAtivas/",["config.xml"]], ["/mnt/opt/Conectpark.TagsInativas/", ["config.xml"]], ["/mnt/var/lib/mysql/", ["conectpark", "ibdata1"]]
]

i = 0

while i < 12:
    for x in target[i][1]:
        path_full = target[i][0] + x
        files = x
        path = target[i][0]
        if os.path.exists(x):
            if os.path.isdir(path_full):
                shutil.rmtree(path_full)
                shutil.copytree(x, path_full)
            else:
                os.remove(path_full)
                shutil.copy(x, path_full)
        if not os.path.exists(path_full):
            if os.path.isfile(x):
                shutil.copy(x, path_full)
            if os.path.isdir(x):
                shutil.copytree(x, path_full)
                
    i = i + 1

