#!/bin/bash

declare -a nms=($(ls /unidade | grep ifcfg | awk -F "-" '{print $2}' ))
adp=($(ls /sys/class/net))
idx=0
    for x in "${nms[@]}"; do
	    adptr=${adp[$idx]}
	touch /mnt/root/etc/udev/rules.d/$x
echo -e "SUBSYSTEM==\"net\", ACTION==add, ATTR{address}==$(cat /sys/class/net/$adptr/address), NAME==\"$x\"" > /mnt/root/etc/udev/rules.d/$x
idx=$idx+1
	
done
