#!/bin/bash -xv

opt=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep opt`
root=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep root`
var=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep var`
home=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep home`

mount $root /unidade/monta
mount $opt /unidade/monta/opt
mount $var /unidade/monta/var
mount $home /unidade/monta/home
mount /dev/sda1 /unidade/monta/boot

chroot /unidade/monta /bin/bash

chown -R mysql.mysql /var/lib/mysql
chown -R conectpark.conectpark /opt/*
dracut -f /boot/initramfs-3.10.0-862.11.6.el7.x86_64.img 3.10.0-862.11.6.el7.x86_64

exit

