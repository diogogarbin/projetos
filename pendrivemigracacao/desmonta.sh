#!/bin/bash

opt=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep opt`
root=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep root`
var=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep var`

cd /unidade/

tar -xvzf bkp.tar.gz

cd /etc/

umount $root
umount $var
umount $opt

