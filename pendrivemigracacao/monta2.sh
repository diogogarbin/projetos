#!/bin/bash

opt=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep opt`
root=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep root`
var=`fdisk -l | grep /dev/mapper | awk '{print $2}' | tr -d ':' | grep var`

fdisk -l
partprobe /dev/sda

sleep 2

#fsck -yfc $root
#fsck -yfc $opt
#fsck -yfc $var


mount $root /mnt/root

mount $opt /mnt/opt

mount $var /mnt/var

cd /mnt/opt
ln -sf /mnt$(readlink /mnt/opt/slt) slt
ln -sf /mnt$(readlink /mnt/opt/conectpark) conectpark
ln -sf /mnt$(readlink /mnt/opt/Conectpark.EnvioMensalistas) Conectpark.EnvioMensalistas
ln -sf /mnt$(readlink /mnt/opt/Conectpark.EnvioTransacoesPendentes) Conectpark.EnvioTransacoesPendentes
ln -sf /mnt$(readlink /mnt/opt/Conectpark.EnvioTransits) Conectpark.EnvioTransits
ln -sf /mnt$(readlink /mnt/opt/Conectpark.SituacaoTag) Conectpark.SituacaoTag
ln -sf /mnt$(readlink /mnt/opt/Conectpark.TagsInativas) Conectpark.TagsInativas
ln -sf /mnt$(readlink /mnt/opt/Conectpark.TagsAtivas) Conectpark.TagsAtivas

