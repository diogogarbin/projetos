#!/bin/bash

echo "INÍCIO DA MIGRAÇÃO DE S.O"

echo "MONTANDO AS UNIDADES"

/unidade/monta.sh

echo "COLETANDO AS INFORMAÇÕES NECESSÁRIOAS"

/unidade/coleta.py

echo "COLETANDO INFORMAÇÕES DE REDE"

/unidade/coleta_rede.py

echo "DESMONTANDO AS UNIDADES"

/unidade/desmonta.sh

echo "INSTALANDO CENTOS7"

/unidade/dd.sh

echo "CONVERTENDO PADRÃO DE REDE"

/unidade/converte_rede.py

echo "MIGRANDO AS INFORMAÇÕES DE REDE"

/unidade/mv_rede.sh

echo "MIGRANDO A NOMENCLATURA DE REDE"

/unidade/altera_placa.sh

echo "MIGRANDO CONFIGURAÇÕES E BASE DE DADOS"

/unidade/transfere.py

echo "O SERVIDOR SERÁ DESLIGADO, AO APAGAR A TELA RETIRE O PENDRIVE, SOMENTE QUANDO A TELA APAGAR"
