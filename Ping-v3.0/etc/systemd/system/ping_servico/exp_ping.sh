#!/bin/bash -xv

tamanho=`ls -lh --block-size=M /opt/reports/log_ping/ping.log | awk '{print $5}' | tr -d "M"` 
script=`ps -ef | grep ping.sh | grep -v grep | awk '{print $2}' | head -1`
arq=/opt/reports/log_ping/ping.log

	if [ $tamanho -ge 80 ] ; then
		mv $arq /opt/reports/log_ping/$(date +'%m-%d-%H:%M')
		mv  /opt/reports/log_ping/$(date +'%m-%d-%H:%M') /opt/LOGS/ping_log/
		cd /opt/LOGS/ping_log/ ; gzip $(date +'%m-%d-%H:%M') ; rm -f $(date +'%m-%d-%H:%M')
		
systemctl stop pingando.service
systemctl restart ping.target
	fi

	if [ $(date +'%H') -eq 00 ]; then
		elif [ ! -f /opt/LOGS/ping_log/$(date +'%m-%d-00') ]; then
		mv $arq /opt/reports/log_ping/$(date +'%m-%d-%H:%M')
		mv  /opt/reports/log_ping/$(date +'%m-%d-%H:%M') /opt/LOGS/ping_log/
		cd /opt/LOGS/ping_log/ ; gzip $(date +'%m-%d-%H:%M') ; rm -f $(date +'%m-%d-%H:%M')


systemctl stop pingando.service
systemctl restart ping.target
	fi
