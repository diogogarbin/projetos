#!/bin/bash -xv

if [ ! -d /opt/reports/log_ping ] ; then

mkdir /opt/reports/log_ping
mkdir /opt/LOGS/ping_log

fi

mount -t tmpfs -o size=100m ramfs /opt/reports/log_ping

if [ ! -f /opt/reports/log_ping/ping.log ]; then

touch /opt/reports/log_ping/ping.log
chmod 755 /opt/reports/log_ping/ping.log

fi

for x in `cat /etc/systemd/system/ping_servico/tds_ip.txt` ; do

ping -i 2 $x | while read pong ; do echo "$(date +'%Y-%m-%d--%H:%M'): $pong" echo "[$x"] >> /opt/reports/log_ping/ping.log ; done &
done

