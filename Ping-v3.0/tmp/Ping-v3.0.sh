#!/bin/bash

#IP Toten

cat /opt/conectpark/config.xml | grep 'IP' | grep -v '127.0.0.1\|Server' | cut -d '>' -f 2 | cut -d '<' -f 1 >> /etc/systemd/system/ping_servico/tds_ip.txt

#IP Antenas

cat /opt/slt/configpista/ifpistas.ini | grep '^ip' | cut -d '=' -f 2 >> /etc/systemd/system/ping_servico/tds_ip.txt

#Acura

if [ -d /opt/local915 ]; then
cat /opt/local915/conf/local915.ini | grep '^IP' | cut -d '=' -f 2 | sed 's/ //g' >> /etc/systemd/system/ping_servico/tds_ip.txt
fi

systemctl daemon-reload
systemctl enable pingando.service
systemctl daemon-reload
systemctl restart crond
#systemctl stop exp.service
#systemctl disable exp.service
